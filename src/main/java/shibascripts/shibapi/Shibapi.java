package shibascripts.shibapi;

import com.google.inject.Inject;
import ninja.leaping.configurate.ConfigurationNode;
import org.slf4j.Logger;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "shibapi",
        name = "Shibapi",
        description = "A common API for my plugins",
        url = "https://gitlab.com/shibascripts",
        authors = {
                "nicholasrobertm"
        }
)
public class Shibapi {

    private static Shibapi instance;

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
    }

    public synchronized Logger getLogger (){
        return logger;
    }

    public static synchronized Shibapi getInstance() {
        return instance;
    }

}
