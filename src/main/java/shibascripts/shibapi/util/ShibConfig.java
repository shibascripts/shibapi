package shibascripts.shibapi.util;

import com.google.inject.Inject;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import shibascripts.shibapi.Shibapi;

import java.io.PrintWriter;
import java.nio.file.Paths;

public class ShibConfig {

    private String modID;
    private String defaultConfig;

    @Inject
    @ConfigDir(sharedRoot = false)
    private String path;

    private YAMLConfigurationLoader loader;

    private ConfigurationNode rootNode;

    public ShibConfig(String modID, String defaultConfig)
    {
        this.modID = modID;
        this.defaultConfig = defaultConfig;
        loader = YAMLConfigurationLoader.builder().setPath(Paths.get(path + ".yml")).build();
        reload();
    }

    public void reload()
    {
        loadConfig();
    }

    public void loadConfig() {

        try{
            if(!Paths.get(path + ".yml").toFile().exists()){
                Paths.get(path + ".yml").toFile().createNewFile();
                PrintWriter pw = new PrintWriter(Paths.get(path + ".yml").toFile());
                pw.println(defaultConfig);
                pw.close();
            }
            rootNode = loader.load();
            loader.save(rootNode);
        }catch(Exception e)
        {
            Shibapi.getInstance().getLogger().error("Problem creating the config");
            Shibapi.getInstance().getLogger().error(String.valueOf(e));
        }

    }

    public ConfigurationNode getRootNode() {
        return rootNode;
    }

}
